import {ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {getValidationMessage} from "../../_core/validators/validators";
import {FormControl} from "@angular/forms";
import {Subject, Subscription} from "rxjs";

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent implements OnInit, OnDestroy {

  @Input() formControlNameValue: FormControl;
  @Input() placeholder: string = '';
  @Input() span: string = '';
  @Input() height: string = '';
  @Input() maxlength: number = 9999;
  @Input() minlength: number = 0;
  public error$: Subject<string> = new Subject();
  private subscription$: Subscription = new Subscription();
  @Output() textareaValueLength = new EventEmitter<number>();

  constructor(private ref: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.handleChanges();
  }

  private handleChanges(): void {
    if (this.formControlNameValue) {
      this.subscription$.add(this.formControlNameValue.statusChanges.subscribe(() => this.getError()));
    }
    this.subscription$.add(this.formControlNameValue.valueChanges.subscribe(() => {
      this.ref.markForCheck();
      this.sendTextareaValueLength(this.formControlNameValue.value.length)
    }));

  }

  public sendTextareaValueLength(length: number): void {
    this.textareaValueLength.emit(length);
  }

  private getError(): void {
    if (this.formControlNameValue.errors && !this.formControlNameValue.errors.hasOwnProperty('required') && this.formControlNameValue.value === '') {
      this.error$.next(undefined);
      return;
    }
    if (this.formControlNameValue.touched) {
      if (this.formControlNameValue.errors) {
        const key = Object.keys(this.formControlNameValue.errors)[0];
        this.error$.next(getValidationMessage(key, this.formControlNameValue.errors[key]));
      } else {
        this.error$.next(undefined);
      }
    }
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}
