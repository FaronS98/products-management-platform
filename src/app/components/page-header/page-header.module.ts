import {NgModule} from '@angular/core';
import {PageHeaderComponent} from "./page-header.component";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common"

@NgModule({
  declarations: [
    PageHeaderComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
  ],
  exports: [
    PageHeaderComponent,
  ]
})
export class PageHeaderModule {
}
