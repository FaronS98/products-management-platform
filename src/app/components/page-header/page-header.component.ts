import {Component, Input, OnInit} from '@angular/core';
import {IMenuDTO} from "../../_core/interfaces/menu.interface";
import {Router} from "@angular/router";
import {AuthService} from "../../_core/services/auth.service";

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent implements OnInit {

  @Input() topMenu: IMenuDTO[] = [];


  constructor(public router: Router,
              private authService: AuthService,) {
  }

  ngOnInit(): void {
    console.log(this.router.url.slice(0,13))
  }

  public logout(): void {
    this.authService.removeToken('adminToken');
    this.router.navigateByUrl('/admin-auth');
  }
}
