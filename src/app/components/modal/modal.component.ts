import {Component, Input, Output, EventEmitter, HostListener} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {

  @Input() width: string = '';
  @Output() modalClosed = new EventEmitter();
  @Output() changePopUpState = new EventEmitter<boolean>();
  public visible = false;

  constructor() {
  }

  public open(): void {
    this.visible = true;
  }

  public close(): void {
    if (this.visible) {
      this.modalClosed.emit();
    }
    this.visible = false;
  }

  @HostListener('document:keydown', ['$event'])
  keyExit(event: KeyboardEvent): void {
    if (event.key === 'Escape') {
      this.close();
    }
  }
}
