import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (this.authService.getToken(route.data.tokenName)) {
      this.navigateByTokenName(route.data.tokenName);
      return false;
    }
    return true;
  }

  private navigateByTokenName(tokenName: 'token' | 'wholesalerToken' | 'adminToken'): void {
    switch (tokenName) {
      case 'adminToken':
        this.router.navigateByUrl('/admin-logged')
        break;
      default:
        this.router.navigateByUrl('/')
    }
  }
}
