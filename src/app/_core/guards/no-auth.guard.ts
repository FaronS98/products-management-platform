import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class NoAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const token: string | null = this.authService.getToken(route.data.tokenName);
    if (token === null) {
      this.router.navigateByUrl('/');
      return false;
    }
    if (!token) {
      this.navigateByTokenName(route.data.tokenName);
      return false;
    }
    return true;
  }

  private navigateByTokenName(tokenName: 'token' | 'wholesalerToken' | 'adminToken'): void {
    switch (tokenName) {
      case 'adminToken':
        this.router.navigateByUrl('/admin-auth/login')
        break;
      default:
        this.router.navigateByUrl('/')
    }
  }
}
