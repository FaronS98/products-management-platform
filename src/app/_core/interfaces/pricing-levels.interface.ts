export interface  IPricingLevelsDTO {
  amount: number,
  price: string
}
