export interface IMenuDTO {
  routerLink: string;
  name: string;
}
