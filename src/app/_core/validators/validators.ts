import {FormGroup, FormControl, FormArray} from '@angular/forms';
import {AbstractControl} from '@angular/forms';

export function getValidationMessage(validator: string, validatorValue?: any): string {
  const messages = {
    required: 'Wymagane',
    email: 'Email jest nieprawidłowy',
    minlength: 'Min długość: ' + validatorValue.requiredLength,
    maxlength: 'Max długość: ' + validatorValue.requiredLength,
    min: 'Min wartość: ' + validatorValue.min,
    max: 'Max wartość ' + validatorValue.max,
  };
  // @ts-ignore
  return messages[validator];
}

export function validateAllFormFields(formGroup: FormGroup | FormArray) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl || (control instanceof FormArray && !(control.controls.length > 0))) {
      control.markAsTouched({onlySelf: true});
      control.updateValueAndValidity({onlySelf: true});
    } else if ((control instanceof FormGroup) || (control instanceof FormArray)) {
      validateAllFormFields(control);
    }
  });
  formGroup.updateValueAndValidity();
}

export function email(AC: AbstractControl) {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (AC.value === null) {
    return null;
  }
  return AC.value.indexOf('@') > 64 || AC.value.length > 255 || !regex.test(AC.value) ? {'email': true} : null;
}

export function matchPassword(FG: FormGroup): void | null {
  const password = FG.get('newPassword')?.value;
  const confirmPassword = FG.get('newPassword2')?.value;
  if (password && confirmPassword) {
    if (password !== confirmPassword) {
      FG.get('newPassword2')?.markAsTouched({onlySelf: true});
      FG.get('newPassword2')?.setErrors({matchPassword: true});
    } else {
      return null;
    }
  }
}
