import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {IUserLoginDTO} from "../interfaces/user-login.interface";

@Injectable({
  providedIn: "root"
})

export class AuthService {

  private adminToken: string | null = '';

  constructor(private router: Router) {
    this.getTokenFromStorage('adminToken');
  }

  private getTokenFromStorage(tokenName: 'adminToken'): void {
    if (!this[tokenName]) {
      this[tokenName] = localStorage.getItem(tokenName);
    }
  }

  private setLocalStorageToken(tokenName: 'adminToken', tokenValue: string | null): void {
    if (tokenValue) {
      this[tokenName] = tokenValue;
      localStorage.setItem(tokenName, tokenValue)
    }
  }

  public removeToken(tokenName: 'adminToken'): void {
    localStorage.removeItem(tokenName);
    this[tokenName] = null;
  }

  public getToken(tokenName: 'adminToken'): string | null {
    return this[tokenName];
  }

  public loginAdmin(data: IUserLoginDTO){
    const randomToken = 'ttttdddd3333'
    this.setLocalStorageToken('adminToken', randomToken);
    this.router.navigateByUrl('admin-logged/add-product')
  }
}
