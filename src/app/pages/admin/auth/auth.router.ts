import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminAuthComponent} from "./auth.component";
import {LoginComponent} from "./login/login.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {AuthGuard} from "../../../_core/guards/auth.guard";

const routes: Routes = [
  {
    path: '',
    component: AdminAuthComponent,
    canActivate: [AuthGuard],
    data: {tokenName: 'adminToken'},
    children: [
      {path: 'login', component: LoginComponent},
      {path: 'password-reset/:id', component: ResetPasswordComponent},
      {path: 'forgot-password', component: ForgotPasswordComponent},
      {path: '**', redirectTo: '/admin-auth/login'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})

export class AdminAuthRouter {
}
