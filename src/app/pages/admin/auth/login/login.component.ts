import {Component} from '@angular/core';
import {AuthService} from "../../../../_core/services/auth.service";
import {Router} from "@angular/router";
import {FormBuilder, Validators} from "@angular/forms";
import {email, validateAllFormFields} from "../../../../_core/validators/validators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public adminLoginForm = this.fb.group({
    email: [null, [Validators.required, email]],
    password: [null, Validators.required]
  });

  constructor(private authService: AuthService,
              private router: Router,
              private fb: FormBuilder) {
  }

  public loginAdmin() {
    if (!this.adminLoginForm.valid) {
      validateAllFormFields(this.adminLoginForm);
      return;
    }
    this.authService.loginAdmin(this.adminLoginForm.value);
  }
}
