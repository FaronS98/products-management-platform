import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AdminAuthComponent} from "./auth.component";
import {AdminAuthRouter} from "./auth.router";
import {LoginComponent} from './login/login.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {InputModule} from "../../../components/input/input.module";

@NgModule({
  bootstrap: [AdminAuthComponent],
  imports: [
    AdminAuthRouter,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    InputModule,
  ],
  declarations: [
    AdminAuthComponent,
    LoginComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
  ],
  providers: [],
})
export class AdminAuthModule {
}
