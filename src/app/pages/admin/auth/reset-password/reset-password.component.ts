import {Component} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {NotifierService} from "angular-notifier";
import {Router} from "@angular/router";
import {matchPassword, validateAllFormFields} from "../../../../_core/validators/validators";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {

  public resetAdminPasswordForm = this.fb.group({
    newPassword: [null, [
      Validators.required,
      Validators.minLength(9),
      Validators.maxLength(20),
      Validators.pattern("^(?:(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).*)$")
    ]],
    newPassword2: [null, [
      Validators.required,
      Validators.minLength(9),
      Validators.maxLength(20),
      Validators.pattern("^(?:(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).*)$")
    ]],
  });

  constructor(private router: Router,
              private notifierService: NotifierService,
              private fb: FormBuilder,) {
  }

  public resetAdminPassword(): void {
    if (!this.resetAdminPasswordForm.valid) {
      validateAllFormFields(this.resetAdminPasswordForm);
      matchPassword(this.resetAdminPasswordForm);
      return;
    }
    this.notifierService.notify('success', 'Hasło zostało zmienione.');
    this.router.navigateByUrl('/admin');
  }
}
