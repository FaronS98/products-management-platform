import {Component} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {email, validateAllFormFields} from "../../../../_core/validators/validators";
import {NotifierService} from "angular-notifier";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  public adminForgotPasswordForm = this.fb.group({
    email: [null, [Validators.required, email]]
  })

  constructor(private fb: FormBuilder,
              private notifierService: NotifierService,) {
  }

  public remindAdminPassword(): void {
    if (!this.adminForgotPasswordForm.valid) {
      validateAllFormFields(this.adminForgotPasswordForm);
      return;
    }
    this.notifierService.notify('info', 'Link do resetu hasła został wysłany na email.');
  }
}
