import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalComponent} from "../../../../components/modal/modal.component";
import {FormBuilder, Validators} from "@angular/forms";
import {IPricingLevelsDTO} from "../../../../_core/interfaces/pricing-levels.interface";
import {validateAllFormFields} from "../../../../_core/validators/validators";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  @ViewChild('addProductModal', {static: true}) addProductModal: ModalComponent;
  public addProductForm = this.fb.group({
    name: [null, Validators.required],
    description: [null, Validators.required],
    pricingLevels: [null, Validators.required],
  });

  public addPriceForm = this.fb.group({
    amount: [null, Validators.required],
    price: [null, Validators.required]
  })

  public pricingLevels: IPricingLevelsDTO[] = [];
  public descriptionLength: number = 0;

  constructor(private fb: FormBuilder,) {
  }

  ngOnInit(): void {
  }

  public addPriceLevel(): void {
    if (!this.addPriceForm.valid) {
      validateAllFormFields(this.addPriceForm);
      return;
    }
    this.pricingLevels.push(this.addPriceForm.value);
    this.addPriceForm.reset();
    this.addPriceForm.markAsUntouched();
  }

  public addProduct(): void {
    if (!this.addProductForm.valid) {
      validateAllFormFields(this.addProductForm);
      return;
    }
  }

  public removePriceLevel(index: number): void {
    this.pricingLevels.splice(index, 1);
  }

  public getTextareaValueLength(length: number) {
    this.descriptionLength = length;
  }
}
