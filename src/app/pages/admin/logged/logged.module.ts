import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AdminLoggedRouter} from "./logged.router";
import {AdminLoggedComponent} from "./logged.component";
import {AddProductComponent} from "./add-product/add-product.component";
import {ProductsListComponent} from "./products-list/products-list.component";
import {ModalModule} from "../../../components/modal/modal.module";
import {InputModule} from "../../../components/input/input.module";
import {TextareaModule} from "../../../components/textarea/textarea.module";
import {CommonModule} from "@angular/common";
import {PageHeaderModule} from "../../../components/page-header/page-header.module";

@NgModule({
  bootstrap: [AdminLoggedComponent],
  imports: [
    CommonModule,
    AdminLoggedRouter,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
    InputModule,
    TextareaModule,
    PageHeaderModule,
  ],
  declarations: [
    AdminLoggedComponent,
    AddProductComponent,
    ProductsListComponent,
  ],
  providers: [],
})
export class AdminLoggedModule {
}
