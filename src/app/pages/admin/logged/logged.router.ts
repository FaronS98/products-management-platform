import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminLoggedComponent} from "./logged.component";
import {AddProductComponent} from "./add-product/add-product.component";
import {ProductsListComponent} from "./products-list/products-list.component";
import {NoAuthGuard} from "../../../_core/guards/no-auth.guard";

const routes: Routes = [
  {
    path: '',
    component: AdminLoggedComponent,
    canActivate: [NoAuthGuard],
    data: {tokenName: 'adminToken'},
    children: [
      {path: 'add-product', component: AddProductComponent},
      {path: 'products-list', component: ProductsListComponent},
      {path: '**', redirectTo: '/admin-logged/add-product'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})

export class AdminLoggedRouter {
}
