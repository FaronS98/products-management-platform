import {Component} from '@angular/core';
import {IMenuDTO} from "../../../_core/interfaces/menu.interface";

@Component({
  selector: 'app-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['./logged.component.scss']
})
export class AdminLoggedComponent {

  public topMenu: IMenuDTO[] = [
    {
      routerLink: '/admin-logged/add-product',
      name: 'Dodaj product',
    },
    {
      routerLink: '/admin-logged/products-list',
      name: 'Lista produktów',
    },
  ];

  constructor() {
  }
}
