import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProductsListComponent} from "./products-list/products-list.component";
import {UserComponent} from "./user.component";
import {UserRouter} from "./user.router";
import {HomePageComponent} from "./home-page/home-page.component";
import {PageHeaderModule} from "../../components/page-header/page-header.module";

@NgModule({
  bootstrap: [UserComponent],
  imports: [
    UserRouter,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PageHeaderModule,
  ],
  declarations: [
    ProductsListComponent,
    HomePageComponent,
  ],
  providers: [],
})
export class UserModule {
}
