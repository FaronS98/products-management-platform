import {Component} from '@angular/core';
import {IMenuDTO} from "../../_core/interfaces/menu.interface";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {

  public topMenu: IMenuDTO[] = [
    {
      routerLink: '/',
      name: 'Strona główna',
    },
    {
      routerLink: '/products-list',
      name: 'Lista produktów',
    },
  ];

  constructor() {
  }
}
