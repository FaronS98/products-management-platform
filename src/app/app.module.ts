import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app.router";
import {UserComponent} from './pages/user/user.component';
import {NotifierModule} from "angular-notifier";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxMaskModule} from "ngx-mask";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NotifierModule.withConfig({}),
    NgxMaskModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
