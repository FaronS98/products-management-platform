import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'admin-auth',
    loadChildren: () => import('./pages/admin/auth/auth.module').then(m => m.AdminAuthModule)
  },
  {
    path: 'admin-logged',
    loadChildren: () => import('./pages/admin/logged/logged.module').then(m => m.AdminLoggedModule)
  },
  {
    path: 'admin',
    redirectTo: 'admin-auth'
  },
  {
    path: '',
    loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule)
  },
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
